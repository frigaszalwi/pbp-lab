from django.shortcuts import render
from django.http.response import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from datetime import datetime, date
from lab_1.models import Friend
from .forms import FriendForm
from django.contrib.auth import authenticate, login
from django.urls import reverse

@login_required(login_url='/admin/login/')

def index(request):
    friends = Friend.objects.all().values()
    response = {'friend': friends}
    return render(request, 'lab3_index.html', response)



def add_friend(request):
    context ={}
  
    # create object of form
    form = FriendForm(request.POST or None, request.FILES or None)
      
    # check if form data is valid
    if form.is_valid():
        # save the form data to model
        form.save()
        return HttpResponseRedirect(reverse('index'))
  
    context['form']= form
    return render(request, 'lab3_form.html', context)