from django.urls import path
from .views import index, xml, json

urlpatterns = [
    path('', index, name='index'),
    path('xml', xml, name='xml'),
    path('jason', json, name='json'),
]