1. Apakah perbedaan antara JSON dan XML?

JSON atau JavaScript Object Notation merupakan penyimpanan informasi yang mudah diakses dan juga terorganisir. Selain itu, JSON juga mudah untuk dipahami manusia

XML adalah bahasa yang bertujuan untuk menyimpan data ataupun transfer data

Kedua hal itu memiliki banyak sekali perbedaan, misalnya :

- Bahasa :
XML : menggunakan bahasa markup yang memiliki tag
JSON : menggunakan format javascript

- Penyimpanan data :
XML : penyimpanan menggunakan tree structure
JSON : penyimpanan menggunakan map dengan key dan valuenya

- Kecepatan sistem :
XML : dalam penguraian, membutuhkan waktu yang banyak sehingga lambat dalam transmisi data
JSON : penguraian data dilakukan oleh JavaScript sehingga transfer data lebih cepat

2. Apakah perbedaan HTML dan XML?

HTML atau HyperText Markup Language adalah sebuah bahasa yang digunakan untuk membangun website

Beberapa perbedaan dari XML dan HTML. misalahnya :
- tujuan :
HTML : digunakan untuk penyajian data
XML : digunakan untul transfer data

- tag :
HTML : tag-tag yang ada terbatas
XML : tag-tag yang ada bisa dikembangankan menjadi tag baru

Referensi : 
https://blogs.masterweb.com/perbedaan-xml-dan-html/
https://www.monitorteknologi.com/perbedaan-json-dan-xml/




